module Document.RemoveDuplicates (
  removeDuplicates,
) where

import BasicPrelude
import Data.Tree
import Markdown.MdLine
import Document.Types
import Document.TreeSection


removeDuplicates::(TreeSection MdLine) -> IO (TreeSection MdLine)
removeDuplicates tree@(Node a []) = return(tree)
removeDuplicates tree@(Node a as) = do
  print "remove duplicate running"
  return(removeChildDuplicates $ removeRootDuplicates tree)

removeChildDuplicates::(TreeSection MdLine) -> TreeSection MdLine
removeChildDuplicates tree@(Node a as) = compose (map transformAndRemove [0..length(as)-1]) tree

removeDecendentsDuplicates:: TreeSection MdLine -> TreeSection MdLine
removeDecendentsDuplicates tree@(Node x xs) = compose (map transformAndRemove [1..length(xs)-1]) tree

compose::[a->a] -> a -> a
compose fs v = foldl (flip (.)) id fs $ v

removeRootDuplicates::TreeSection MdLine -> TreeSection MdLine
removeRootDuplicates tree@(Node a []) = Node a []
removeRootDuplicates tree@(Node a as) = Node a $ map (removeSection a) as

transformAndRemove::Int -> TreeSection MdLine -> TreeSection MdLine
transformAndRemove int tree = childToParentPivot int $ removeDecendentsDuplicates $ removeRootDuplicates $ parentToChildPivot int tree

parentToChildPivot::Int -> Tree a -> Tree a
parentToChildPivot int (Node x xs) = addBranch (xs !! int) (Node x (listWithout int xs))

childToParentPivot::Int -> Tree a -> Tree a
childToParentPivot int (Node x ((Node p ps):xs)) = Node p ((\(x,y) z -> x ++ (z:y)) (splitAt int ps) (Node x xs))

addBranch::Tree a -> Tree a -> Tree a
addBranch (Node x xs) a = Node x (a:xs)

listWithout::Int -> [Tree a] -> [Tree a]
listWithout int [] = []
listWithout int as
  | int == 0 = tail as
  | otherwise = take (int) as ++ drop (int + 1) as

removeSection:: MdLine -> TreeSection MdLine -> TreeSection MdLine
removeSection x (Node a as)
  | isLine x = Node a as
  | isLine a = Node a as
  | (identity x) == (identity a) = Node (toReference a) []
  | otherwise = Node a $ map (removeSection x) as
