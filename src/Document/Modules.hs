{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedStrings #-}
module Document.Modules
  ( buildDocument
  , buildModuleKit
  , ModuleBlueprint(..)
  , getModuleBlueprint
  , MdModule(..)
  , DocumentBuilder(..)
  , MarkownDocumentBuilder(..)
  , MarkdownPrintableDocumentBuilder(..)
  , MarkdownHtmlDocumentBuilder(..)
  --getPotentialSections,
  --getPotentialSection,
  --buildModule,
  --replaceHeadingReferences,
  --referencesToReferenceAndFilePaths
  --replaceHeadingReference
  )
where
import BasicPrelude
import Document.RemoveDuplicates
import qualified Data.Sequence as Seq
import           Document.Assembler
import           Document.Blueprints
import           Document.Sections
import           Data.Tree
import           Data.Maybe
import           Document.Types
import qualified          Data.List                     as L
import           Data.List.Utils
import           Text.ParserCombinators.Parsec as PC
import           System.FilePath
import           System.Directory
import           Document.TreeSection
import           Markdown.Parser
import           ReferencedTrees.ReferencedTree
import           Markdown.MdLine
import           Document.PrintableMdSection
import qualified Data.Set                      as Set
import           Data.List.Split
import qualified Data.Sequence                 as Seq
import qualified Data.Text                     as T

data MdModule = MdModule {document:: (TreeSection MdLine) , appendix::[(TreeSection MdLine)] }deriving Eq
data PrintableMdModule = PrintableMdModule {pDocument:: (PrintableMdSection) , pAppendix::[PrintableMdSection] }deriving (Eq)
--data HtmlModule = HtmlModule {hDocument::(HtmlMdSection), hAppendix::[HtmlMdSection]} deriving Eq

modToMdMod (Module s app) = MdModule s app

modToPrintableMdMod (Module ps papp) = PrintableMdModule ps papp

--modToHtmlMod (Module ps papp) = HtmlModule ps papp

toPrintableSectionKit
  :: SectionKit (Tree MdLine) -> SectionKit (PrintableMdSection)
toPrintableSectionKit = fmap (\(s, path) -> (toPrintable s, path))

modblockToPrintable
  :: (ModuleBlock (Tree MdLine) -> ModuleBlock PrintableMdSection)
modblockToPrintable (s, xs) = (toPrintable s, map toPrintableSectionKit xs) where
--type ModuleBlock s = (s, AppendixKit s)

modBlockAndPathToPrintable
  :: (ModuleBlock (Tree MdLine), Path)
  -> (ModuleBlock (PrintableMdSection), Path)
modBlockAndPathToPrintable (modblock, path) =
  (modblockToPrintable modblock, path)

modKitToPrintable :: (ModuleKit (Tree MdLine)) -> ModuleKit (PrintableMdSection)
modKitToPrintable = fmap modBlockAndPathToPrintable
--type ModuleKit s = Tree (ModuleBlock s, Path)

instance ModuleClass MdModule where
  buildModule filepath path = fmap modToMdMod $ do --What if path is empty?
    let direction = Direction [] filepath path
    modKit <- buildModuleKit direction
    return (assembleModule modKit)

  moduleToString m = return (concat
    [ start
    , sectionToString BgOne (Document.Modules.document m) (fst ids)
    , append
    , end
    ] )  where
    append = concat
      ["<div class=\"section\">\n", appendHead, appendBody, "</div>"]     where
    appendHead
      = "<div class=\"sectionHeading\" onclick=\"foldAppendix()\"><h2>Appendix</h2></div>\n<script>foldAppendix=fold('Appendix')</script>\n"
    appendBody = concat [startBody, mainBody, endBody]
    startBody =
      "\n<div class=\"sectionBody " ++ textShow BgOne ++ "\" id=\"Appendix\">\n"
    mainBody = sectionsToString BgOne (Document.Modules.appendix m) (snd ids)     where
    endBody = "</div>\n</div>"
    supply  = [] :: [Integer]
    ids     = (0 : supply, 1 : supply)
    start
      = "<script> function fold(y) { var x = document.getElementById(y) ; if(x.style.display === \"none\"){x.style.display = \"block\";}else {x.style.display = \"none\"; } } </script><script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML' async></script>"
    end = ""
{--
instance DocumentBuilder MarkdownHtmlDocumentBuilder where
  buildDocument m path filepath = do
    print $ "build Document running on: " ++ filepath
    mod <- (buildModule filepath path)::IO (HtmlModule)
    htmlModuleToStringIO (mod::HtmlModule)
--}

instance ModuleClass PrintableMdModule where
  buildModule filepath path = fmap modToPrintableMdMod $ do --What if path is empty?
    print "build printable module running"
    let direction = Direction [] filepath path
    modKit <- buildModuleKit direction
    let printableModKit = modKitToPrintable modKit
    return (assemblePrintableModule printableModKit)   where
    toPrintableModblock (x, xs) = (toPrintable x, fmap (fmap toPrintable) xs)

  moduleToString m = do
    let n = (\(PrintableMdSection x ) -> x) $ Document.Modules.pDocument m
    main <- (fmap PrintableMdSection $ removeDuplicates n)
    return (concat [ start
      , (sectionToString BgOne main) (fst ids)
      , "\n\n# Appendix\n\n"
      , sectionsToString BgOne (Document.Modules.pAppendix m) (snd ids)
      , end
      ] )  where
        supply = [] :: [Integer]
        ids    = (0 : supply, 1 : supply)
        start  = ""
        end    = ""



data ModuleBlueprint s d = ModuleBlueprint{
  mainSection:: s,
  subModDirections:: [Direction],
  appendixDirections:: [Direction]
} deriving(Show, Eq)

data SectionBlueprint s d = SectionBlueprint{
  sec:: s,
  subSecDirections:: [Direction]
} deriving(Show, Eq)

class DocumentBuilder d where
  buildDocument:: d-> Path -> FilePath -> IO (Text)
--This function is not a recursive building block it is the final stage

data MarkownDocumentBuilder = MarkdownDocumentBuilder
data MarkdownPrintableDocumentBuilder = MarkdownPrintableDocumentBuilder
data MarkdownHtmlDocumentBuilder = MarkdownHtmlDocumentBuilder

instance DocumentBuilder MarkownDocumentBuilder where
  buildDocument m path filepath = do
    print $ "build Document running on: " ++ filepath
    mod <- (buildModule filepath path) :: IO (MdModule)
    modString <- moduleToString (mod :: MdModule)
    return (modString)

instance DocumentBuilder MarkdownPrintableDocumentBuilder where
  buildDocument m path filepath = do
    print $ "build Document running on: " ++ filepath
    mod <- (buildModule filepath path) :: IO (PrintableMdModule)
    modString <- moduleToString (mod :: PrintableMdModule)
    return (modString )

buildModuleKit :: (Eq s, Section s) => Direction -> IO (ModuleKit s)
buildModuleKit direction = do
  blueprint   <- getModuleBlueprint direction --Get blue print should contain nothing recursive
  appendixKit <- buildAppendixKit (appendixDirections blueprint)
  let mainModBlock = ((mainSection blueprint, appendixKit), mainPath direction)
  subModBlocks <- (sequence $ map buildModuleKit (subModDirections blueprint))

  return (Node mainModBlock subModBlocks)

buildAppendixKit :: (Eq s, Section s) => [Direction] -> IO (AppendixKit s)
buildAppendixKit directions = do
  sectionKits <- sequence $ map buildSectionKit directions
  return (sectionKits)

buildSectionKit :: (Eq s, Section s) => Direction -> IO (SectionKit s)
buildSectionKit direction = do
  blueprint <- getSectionBlueprint direction
  let subSectionDirections = subSecDirections blueprint
  let mainSection          = sec blueprint
  subSectionKits <- sequence $ map buildSectionKit subSectionDirections
  return (Node (mainSection, (mainPath direction)) subSectionKits)

getSectionBlueprint
  :: (Eq s, Section s) => Direction -> IO (SectionBlueprint s d)
getSectionBlueprint direction = do
  let fp = Document.Blueprints.filepath direction
  file                <- readFile fp
  potentialbp          <- getPotentialSectionBlueprint fp file direction
  subSectionDirections <- pDirectionsToDirections
    (pSubSecDirections potentialbp)
  return (SectionBlueprint (pSec potentialbp) subSectionDirections)

getModuleBlueprint :: (Eq s, Section s) => Direction -> IO (ModuleBlueprint s d)
getModuleBlueprint direction = do
  let fp = Document.Blueprints.filepath direction
  file       <- readFile fp
  potentialbp <- getPotentialModuleBlueprint fp file direction
  pb          <- (pModBlueprintToModBlueprint potentialbp)
  return pb


pModBlueprintToModBlueprint
  :: (Eq s, Section s)
  => PotentialModuleBlueprint s d
  -> IO (ModuleBlueprint s d)
pModBlueprintToModBlueprint potentialbp = do
  subModuleDirections <-
    (pDirectionsToDirections (pSubModDirections potentialbp))
  appendixDirections <- pDirectionsToDirections
    (pAppendixDirections potentialbp)
  return
    (ModuleBlueprint (pmainSection potentialbp)
                     subModuleDirections
                     appendixDirections
    )

pDirectionsToDirections :: [PotentialDirection] -> IO [Direction]
pDirectionsToDirections pdirs = do
  sequence $ map onlyOneSection (pdirs)

onlyOneSection :: PotentialDirection -> IO (Direction)
onlyOneSection pd = do
  filepath <- onlyOneFile (pFilepaths pd) (pPathToSection pd)
  return (Direction mainPath filepath pathToSection) where
  mainPath      = pMainPath pd
  pathToSection = pPathToSection pd

onlyOneFile :: Set.Set FilePath -> Path -> IO FilePath
onlyOneFile filepaths path = do
  print "onlyOneFile Running MustCheck If Original File is in list"
  print filepaths
  print $ show path
  boolList <- sequence $ map (doesContainSection path) (Set.toList filepaths)
  let zipped   = zip boolList (Set.toList filepaths)
  let filtered = map snd (filter fst zipped)
  let one      = getOneFromList filtered
  print $ "In file " ++ one
  return (one)

doesContainSection :: Path -> FilePath -> IO (Bool)
doesContainSection path fpath = do
  myBool <- doesPathExist fpath
  if myBool
    then do
      s <-
        (fmap (searchSections path . stringToSections) $ readFile fpath) :: IO
          (Maybe (TreeSection MdLine, Path))
      if isNothing s then return False else return True
    else return False

getOneFromList :: (Tshow a) => [a] -> a
getOneFromList []       = error "Error in get one from list nothing in list"
getOneFromList (x : []) = x
getOneFromList (x : xs) = error $ "No unique item found" ++ (T.unpack  $ textShow (x : xs))
