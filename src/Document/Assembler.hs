{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FunctionalDependencies #-}
module Document.Assembler
  ( assembleModule,
    SectionKit,
    AppendixKit,
    ModuleBlock,
    ModuleKit,
    assemblePrintableModule
    --assembleHtmlModule
  ) where

import Document.Sections
import Prelude
import Document.Types
import Data.Tree
import Text.ParserCombinators.Parsec as PC
import Data.Maybe
import Data.List as L
import qualified Data.Text as T
import Data.Maybe
import System.FilePath
import System.IO
import Document.PrintableMdSection
--import Document.HtmlMdSection

type SectionKit s = Tree (s, Path)

type AppendixKit s = [SectionKit s]

type ModuleBlock s = (s, AppendixKit s)

type ModuleKit s = Tree (ModuleBlock s, Path)



modBIntoModB::(Section s) => Into (ModuleBlock s)
modBIntoModB ((subSec, subAppKit), subPath) ((mainSec, mainAppKit), oldPath) = 
    (newModBlock , oldPath) where
      newModBlock = (newSec, newApp) -- ::ModuleBlock s
      newSec = fst $ sectionIntoSection (subSec, subPath)  (mainSec, oldPath)
  
      newApp = mainAppKit ++ subAppKit

manyIntoOne:: Into a -> [(a, Path)] -> (a, Path) -> (a,Path)
manyIntoOne into subParts main = L.foldr into main subParts

manyIntoOneRecursive:: Into a -> Tree (a, Path) -> (a, Path) 
manyIntoOneRecursive into (Node x []) = x
manyIntoOneRecursive into (Node x xs) = manyIntoOne into assembled x where
  assembled = (map (manyIntoOneRecursive into) xs)

manyIntoOneRecursiveAndRemovePath:: Into a -> Tree (a, Path) -> a
manyIntoOneRecursiveAndRemovePath into tree = fst $ (manyIntoOneRecursive into tree)

assembleModule::(Section s) => ModuleKit s  -> Module s
assembleModule tree = Module main appendix where
  main = fst modPrimitive
  appendix = map (manyIntoOneRecursiveAndRemovePath sectionIntoSection) $ appendixSectionKits
  appendixSectionKits = snd modPrimitive 
  modPrimitive = (manyIntoOneRecursiveAndRemovePath modBIntoModB tree)

assemblePrintableModule::ModuleKit PrintableMdSection  -> Module PrintableMdSection
assemblePrintableModule tree = Module main appendix where
  main = fst modPrimitive
  appendix = map (manyIntoOneRecursiveAndRemovePath sectionIntoSection) $ appendixSectionKits
  appendixSectionKits = snd modPrimitive 
  modPrimitive = (manyIntoOneRecursiveAndRemovePath modBIntoModB tree)

{--
assembleHtmlModule::ModuleKit HtmlMdSection  -> Module HtmlMdSection
assembleHtmlModule tree = Module main appendix where
  main = fst modPrimitive
  appendix = map (manyIntoOneRecursiveAndRemovePath sectionIntoSection) $ appendixSectionKits
  appendixSectionKits = snd modPrimitive 
  modPrimitive = (manyIntoOneRecursiveAndRemovePath modBIntoModB tree)
  --}
