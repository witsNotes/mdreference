{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FunctionalDependencies #-}
module Document.Sections(
  sectionsToString,
  showId,
  splitId,
  splitIdL,
  Into(..),
  Color(..),
  ModuleClass(..),
  Module(..),
  Section(..),
  isEmptySections,
  searchSections,
  listMatchingFilePaths,
  getMatchingSubSection,
    ) where
import Document.Types
--import References.Types
import Data.Tree
import Text.ParserCombinators.Parsec as PC
import Data.Maybe
import BasicPrelude
import Data.Text(pack)
import System.FilePath
import System.IO

class ModuleClass m where
  buildModule::String -> Path -> IO(m)
  moduleToString:: m -> IO (Text)



data Module s = Module {document:: s , appendix::[s] }deriving Eq

type Into a = (a, Path) -> (a, Path) -> (a, Path)

data Color = BgOne | BgTwo


instance Enum Color where
  succ BgOne = BgTwo
  succ BgTwo = BgOne

instance Tshow Color where
  textShow BgOne = "background-color-one"
  textShow BgTwo = "background-color-two"


showId ints = pack $ concat $ intersperse "_" $ map show ints

splitId::[Integer] -> ([Integer], [Integer])
splitId xs = (0:xs, 1:xs)

splitIdL::[Integer]->[[Integer]]
splitIdL xs = zipWith (:) [0..] (repeat xs)

class (Tshow s) => Section s where
  searchSection :: Path -> s -> Maybe (s, Path)
  followPath :: Path -> (s, Path) -> Maybe (s, Path)
  isEmptySection :: s -> Bool
  stringToSections:: Text -> [s]
  findSection:: Text -> [Text]  -> (s, Path)
  listSectionAppendixReferences:: s-> [Reference]
  sectionIntoSection:: Into s-- It needs to be this way for currying to be the mot elegent
  listHeadingReferencePaths::s -> Path -> [(Path, Reference)]
  sectionToString::Color -> s -> [Integer] -> Text
  listImports::[s] -> [Import]
  addIds:: Text -> Path -> s -> s
  
sectionsToString::(Section s) => Color -> [s] -> [Integer]-> Text
sectionsToString color sections ints = concat $ zipWith (\f a -> f a) ( map (sectionToString  color) sections) (splitIdL ints)

isEmptySections ::(Section s) => [s] -> Bool
isEmptySections sections
        | all isEmptySection sections = False
        |otherwise = True where

searchSections :: (Section s) => Path -> [s] -> Maybe (s, Path)
searchSections path sections = oneOnly (map (searchSection path) sections)

listMatchingFilePaths:: (Section s) => Reference -> [Import] -> String -> [String]
listMatchingFilePaths reference imports dir =
  map (dir </>)  $ map filepath $ listMatchingImports reference imports

getMatchingSubSection::(Section s) => Path -> [s] -> (s, Path)
getMatchingSubSection path sections = fromJust $ oneOnly (map (searchSection path) sections)
