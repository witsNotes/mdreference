{-#LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-#LANGUAGE MonoLocalBinds #-}
--The Main Point of this module is to try and remove unessesary work from IO module operations
--Assembly provides a way to assemble a number of sections and modules which have already been captured
--AssemblyKits are a list of all needed modules and sections with instructions on how to fit them together
--It provides functions to asssemble sections and modules which are already in the program
--ShopingLists provide all the information necesary to build a blueprint and materials, th

module Document.Blueprints (
  Direction(..),
  PotentialDirection(..),
  PotentialSectionBlueprint(..),
  PotentialModuleBlueprint(..),
  getPotentialModuleBlueprint,
  getPotentialSectionBlueprint
) where

import Path
import Document.Sections
import BasicPrelude
import Document.Types
import Data.Tree
import Text.ParserCombinators.Parsec as PC
import Data.Maybe
import qualified Data.List as L
import qualified Data.Text as T
import Data.Maybe
import System.FilePath
import Markdown.Parser
import qualified Data.Set as Set 

instance Show (String -> Either ParseError d) where
  show x = "Parser"

instance Eq (String -> Either ParseError d) where
  (==) a b = True

data Direction = Direction{
  mainPath::Path, --This is the path showing where the section must be slotted into a larger section
  filepath::FilePath,
  pathToSection::Path
} deriving (Show, Eq) 

data PotentialDirection = PDirection{
  pMainPath::Path, --This is the path showing where the section must be slotted into a larger section
  pFilepaths::Set.Set FilePath,
  pPathToSection::Path
} deriving (Show)

data PotentialModuleBlueprint s d= PModuleBlueprint{
  pmainSection:: s,
  pSubModDirections:: [PotentialDirection],
  pAppendixDirections:: [PotentialDirection]
} deriving(Show)

data PotentialSectionBlueprint s d= PSectionBlueprint{
  pSec :: s,
  pSubSecDirections :: [PotentialDirection]
} deriving(Show)

data SectionPrimitive s = SectionPrimitive{
  main::s,
  imps::[Import],
  directories::Set.Set FilePath
} deriving(Show)

getSecPrimitive::(Section s) => String -> Text -> Direction -> IO(SectionPrimitive s)
getSecPrimitive filename file direction = do
  print "get sec primitvie running"
  acceptLoc <- acceptedLocations 
  let directories = Set.fromList $ (takeDirectory $ takeFileName $ Document.Blueprints.filepath direction):acceptLoc 
  return (SectionPrimitive main imps directories) where
    sections = stringToSections $ file
    main = addIds (T.pack filename) (init (path)) sec
    sec = fst $ getMatchingSubSection (pathToSection direction) sections
    path = snd $ getMatchingSubSection (pathToSection direction) sections 
    imps = (Import "" (Document.Blueprints.filepath direction)) : (listImports sections)
  
getPotentialModuleBlueprint::(Section s) =>  String -> Text -> Direction -> IO (PotentialModuleBlueprint s d)
getPotentialModuleBlueprint filename file direction = do
  print "get Potential module blueprint running"
  secPrim <- getSecPrimitive filename file direction
  let main' = main secPrim
  let dSubSecs = getPSectionDirections secPrim
  let dApp = getPotentialAppendixSections secPrim
  return (PModuleBlueprint main' dSubSecs dApp) where

getPotentialSectionBlueprint::(Section s) => String -> Text -> Direction -> IO (PotentialSectionBlueprint s d)
getPotentialSectionBlueprint filename file direction = do 
  secPrim <- getSecPrimitive filename file direction
  let dSubSecs = getPSectionDirections secPrim
  return (PSectionBlueprint (main secPrim)  dSubSecs) where
  
getPSectionDirections::(Section s) => SectionPrimitive s -> [PotentialDirection]
getPSectionDirections secPrim = makePDir mainPaths filepaths sectionPaths where
  mainPaths = fst $ unzip headingRefPaths
  sectionPaths = map path $ snd $unzip headingRefPaths
  references =  snd $unzip headingRefPaths::[Reference]
  filepaths = map Set.fromList $ referencesToFilePaths secPrim references 
  headingRefPaths = listHeadingReferencePaths (main secPrim) []

getPotentialAppendixSections::(Section s) => SectionPrimitive s -> [PotentialDirection]
getPotentialAppendixSections secPrim = makePDir mainPaths filepaths paths where
  mainPaths = repeat []
  paths = map path (listSectionAppendixReferences (main secPrim))
  references =  (listSectionAppendixReferences (main secPrim))::[Reference]
  filepaths = map Set.fromList $ referencesToFilePaths  secPrim references 

referencesToFilePaths::(Section s) => SectionPrimitive s->[Reference] -> [[FilePath]]
referencesToFilePaths secPrim refs = filepaths where
  matchingImps = map (\x -> listMatchingImports x (imps secPrim)) refs::[[Import]]
  matchingImpsWithDir= map (addDirectoriesToImports (Set.toList $ directories secPrim)) matchingImps::[[Import]]
  filepaths =  map (map Document.Types.filepath) matchingImpsWithDir


makePDir:: [Path] -> [ Set.Set FilePath] -> [Path] -> [PotentialDirection]
makePDir mainPaths filepaths sectionPaths = pds where 
  zipped = zip3 mainPaths filepaths sectionPaths
  tuppleToPD (a ,c ,d) = PDirection a c d
  pds = map tuppleToPD zipped

