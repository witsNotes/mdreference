{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

module Document.PrintableMdSection
  (PrintableMdSection(..) ,
   toPrintable
  ) where

import BasicPrelude
--import Data.List 
import Document.Types
import Data.Tree
import Document.Sections
import Document.TreeSection
import Markdown.MdLine
import Markdown.MdLineDocLine
import ReferencedTrees.ReferencedTree (treeIntoTree, pruneTree)
data PrintableMdSection = PrintableMdSection (Tree MdLine) deriving (Eq)

instance Tshow PrintableMdSection where
  textShow (PrintableMdSection s) = "PrintableMDSection" ++ textShow s

instance Section PrintableMdSection where 
  searchSection path (PrintableMdSection x) = fmap (\(x, y) -> (PrintableMdSection x, y)) (searchSection path x) 
  followPath path (PrintableMdSection x, pTwo) = fmap (\(x, y) -> (PrintableMdSection x, y)) (followPath path (x, pTwo) )
  isEmptySection (PrintableMdSection x) = isEmptySection x
  stringToSections path  = map (\x -> PrintableMdSection x) (stringToSections path)
  findSection file path =  findSection file path 
  listSectionAppendixReferences (PrintableMdSection x) = listSectionAppendixReferences x
  sectionIntoSection ((PrintableMdSection newSec), path) ((PrintableMdSection oldSec), oldPath) =
    (\(x, y) -> (PrintableMdSection x, y)) $ sectionIntoSection (newSec, path) (oldSec, oldPath) where
  listHeadingReferencePaths(PrintableMdSection s) = listHeadingReferencePaths s
 
  sectionToString _ (PrintableMdSection tree@(Node impH@(ImportedHeading l n id) xs)) _ = 
      concat $ (textShow impH):body  ++ [end] where
          body =  zipWith (\f a -> f a) sections ids
          sections = map (sectionToString (BgOne) . toPrintable) $ branches $ pruneTree tree
          ids = (repeat [0])
          branches (Node x xs) = xs 
          end = "\n**End of import**(" ++ n ++")\n"
  sectionToString _ (PrintableMdSection tree@(Node x xs)) _ = 
    concat $ (textShow x):body where
        body =  zipWith (\f a -> f a) sections ids 
        sections = map (sectionToString (BgOne) . toPrintable) $ branches $ pruneTree tree
        ids = (repeat [0])
        branches (Node x xs) = xs


toPrintable::(TreeSection MdLine) -> PrintableMdSection
toPrintable x = PrintableMdSection x
