{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE OverloadedStrings #-}
module Document.Types(
  DocLine(..),
  Reference(..),
  Import(..),
  referenceMatchesImport,
  listMatchingImports,
  Path,
  addDirectoriesToImports,
  oneOnly,
  remainingPath,
  pathFromTopLevelHeading,
  firstStep,
  Tshow(..)
  ) where

import BasicPrelude
import System.FilePath
import Data.Tree
import qualified Data.Text as T
import Data.Maybe
import ReferencedTrees.ReferencedTree
-- | These are all the types used in building the tree out of a document. Imports and lines are just parsed as lines becouse none of the tree operations need them

class Tshow a where
  textShow:: a -> Text

instance (Tshow a, Tshow b) => Tshow (a,b) where
  textShow (a,b) = "(" ++ textShow a ++ "," ++ textShow b ++")"

instance (Tshow a) => Tshow [a] where
  textShow xs = "[" ++ (concat $ intersperse "," $ map textShow xs) ++"]"

instance (Tshow a) => Tshow (Tree a) where
  textShow (Node x []) = "Node " ++ textShow x
  textShow (Node x xs) = "Node " ++ textShow x ++ textShow xs

instance Tshow Text where
  textShow = id

instance Tshow Char where
  textShow c = T.pack [c]

class(Ord a, Tshow a) => DocLine a where
  toImportHeading::a -> a
  isHeadingReference:: a -> Bool
  getHeadingReference:: a -> Reference
  isHiddenHeading:: a -> Bool
  isNotEmptyLine:: a -> Bool      
  name:: a -> Text
  toText:: a -> Text
  isLine:: a -> Bool
  listAppendixReferences:: a -> [Reference] 
  listInlineReference::a -> [Reference]
  hiddenHeadingToHeading::a -> a
  getImport::a -> Import
  addId::Text -> Path -> a -> a
  toReference:: a -> a
  -- This function must take in a string so that it also works with appendix reference
  --This function is used to determine if a topLevelHeading is being called.

--type Path = [String]

pathFromTopLevelHeading::Path -> Bool
pathFromTopLevelHeading path  = ((not $null path) && path !! 0 == "~")

firstStep::Path -> Text
firstStep path 
  | null (path) = error "Called first step on empty list"
  | otherwise = head path where

remainingPath:: Path -> Path
remainingPath path = tail $ path

data Reference = Reference{ origin::String, path::Path} deriving Show

data Import = Import{sudoname:: String, filepath::FilePath} deriving (Show ,Eq)

referenceMatchesImport:: Reference -> Import -> Bool
referenceMatchesImport ref imp 
  | origin ref == filepath imp = True
  | origin ref == sudoname imp = True
  | origin ref == "" = True
  | otherwise = False

listMatchingImports :: Reference -> [Import] -> [Import]
listMatchingImports reference imps = filter (referenceMatchesImport  reference) imps 

addDirectoryToImport:: FilePath -> Import -> Import
addDirectoryToImport dir (Import sudo fp) =  Import sudo (dir </> fp) 

addDirectoriesToImports:: [FilePath] -> [Import] -> [Import]
addDirectoriesToImports dirs imps = [addDirectoryToImport d i | d <- dirs, i <- imps]

oneOnly ::(Tshow a) => [Maybe a] ->  Maybe a
oneOnly xs
  | (length $catMaybes xs) == 0 = Nothing
  | (length $catMaybes xs) == 1 = listToMaybe $catMaybes xs
  | otherwise = error $ "multiple possible trees found in OnlyOne ++ LIST:" ++ (concatMap (T.unpack . textShow) $ catMaybes xs)



