{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Document.TreeSection(
    TreeSection(..)
  ) where

import BasicPrelude
import Document.Types
import ReferencedTrees.ReferencedTree
import qualified Data.Text as T
import Data.Tree
import Document.Sections
import qualified Data.List as L
import Data.Either
import Data.Maybe
import Markdown.MdLine
import Markdown.MdLineDocLine
import Markdown.Parser

type TreeSection a = Tree a 


instance Section (Tree MdLine) where

  followPath = ReferencedTrees.ReferencedTree.followPath 

  searchSection path tree = searchTree path tree 

  isEmptySection (Node x xs) = (not $ isNotEmptyLine x ) && isEmptySections xs 

  stringToSections s =  foldForest' $ rights $ map (parserMarkdown . T.unpack) $ lines s

  findSection file path = fromMaybe myError potentialSection where --find something from maybe which will allow error reporting
    potentialSection = oneOnly potentialSections 
    potentialSections = (L.map (searchSection path)$ stringToSections $ file)
    myError = error " Error in build module No section was found matchinng the one searched for" 

  listSectionAppendixReferences (Node x []) = (listAppendixReferences x)
  listSectionAppendixReferences (Node x xs) = listAppendixReferences x  ++ referencesFromForest where
    referencesFromForest = (concatMap listSectionAppendixReferences xs)
  
  --This does not try and find the head as this should already be kept track of by constructing a full tree
  --It is confusing do not use find path start this function should never need it!
  sectionIntoSection (newSec, path) (oldSec, oldPath) =
    (treeIntoTree (preperForDisplay newSec, path) (oldSec), oldPath) where
        preperForDisplay = toImportSection . pruneTree . revealHiddenTree where
          toImportSection (Node x xs) = Node (toImportHeading x) xs
  
  
  listHeadingReferencePaths (Node x []) path 
    | isHeadingReference x = [(path ++ [(Document.Types.name x)], getHeadingReference x)]
    | otherwise = []
  listHeadingReferencePaths (Node x xs) path 
    | isHeadingReference x = [(path ++ [(Document.Types.name x)] , getHeadingReference x)]
    | otherwise =  concatMap (\y -> listHeadingReferencePaths y (path ++[(Document.Types.name x)])) xs


  sectionToString color tree@(Node h@(ImportedHeading x y id) xs) supply  = 
      script ++ "<div class=\"import\">\n" ++ head ++ body ++ "</div>" where
          script = "<script> function fold" ++ showId id ++ "() {fold('" ++showId id ++"')} </script>"
          head = "<div class=\"importHeading\" onclick=\"fold"++ showId id ++"()\"><h2>" ++ Document.Types.name h ++ "</h2></div>"
          body = startBody ++ mainBody ++ endBody
          startBody =  "\n<div class=\"importBody " ++ textShow color ++"\" id=\"" ++ showId id ++"\">\n"
          mainBody = concat $ zipWith (\f a -> f a) (map (sectionToString (succ color)) $ branches $ pruneTree tree) idlist
          branches (Node x xs) = xs
          endBody="\n</div>\n"
          ids = splitId supply 
          idlist = splitIdL (snd ids)
          id = fst ids
 
  sectionToString _ (Node x []) _ = textShow x
  sectionToString color tree@(Node x xs) supply = 
      script ++ "<div class=\"section\">\n" ++ head ++ body ++ "</div>" where
          script = "<script> function fold" ++ showId id ++ "() {fold('" ++ showId id ++"')}</script>"
          head = "<div class=\"sectionHeading\" onclick=\"fold"++ showId id ++"()\"><h2>" ++ Document.Types.name x  ++ "</h2></div>"
          body = startBody ++ mainBody ++ endBody
          startBody =  "\n<div class=\"sectionBody " ++ textShow color ++"\" id=\"" ++ showId id ++"\">\n"
          mainBody = concat $ zipWith(\f a -> f a) (map (sectionToString (succ color) ) $ branches $ pruneTree tree) idlist 
          branches (Node x xs) = xs
          endBody="\n</div>\n"
          ids = splitId supply 
          idlist = splitIdL (snd ids)
          id = fst ids

        
  listImports sections
    | isNothing importSection = []
    | otherwise = map getImport $ filter isNotEmptyLine $ extractBody $ fromJust importSection where
      extractBody = tail . Data.Tree.flatten
      importSection  = fmap fst $ searchSections (["Imports"]) sections

  addIds filename path (Node x []) = Node x []
  addIds filename path (Node x xs) = Node y ys where
    y = (addId filename (path ++ [Document.Types.name x]) x) 
    ys = map (addIds filename (path ++ [Document.Types.name x])) xs

    
foldForest' :: (Ord a) => [a] -> Forest a
foldForest' [] = []
foldForest' (x:[]) = [Node x []]
foldForest' (x:lx) = (Node x (foldForest' (takeWhile (> x) lx))): (foldForest' $ dropWhile ( >x) lx)
