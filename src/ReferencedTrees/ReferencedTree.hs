module ReferencedTrees.ReferencedTree (
  Path,
  Ref(..),
  isHiddenTree,
  isReferenceTree,
  pruneTree,
  followPath,
  searchTree,
  searchPathStart,
  treeIntoTree,
  revealHiddenTree,
  indentTree
) where

import Data.Tree
import Data.Maybe
import BasicPrelude

type Path = [Text]

pathFromTopLevel::Path -> Bool
pathFromTopLevel path  = ((not $null path) && path !! 0 == "~")

class Ref a where
  reference::(Text) -> a -> Bool
  hidden::a -> Bool
  isRef::a->Bool
  indent:: a -> a
  refEmpty:: a
  reveal:: a -> a
  name::a -> Text

indentTree::(Ref a) => Tree a -> Tree a
indentTree = fmap indent

-- Can probably be made private at end
isHiddenTree::(Ref a) => Tree a -> Bool
isHiddenTree (Node x xs) = hidden x

-- Can probably be made private at end
isReferenceTree::(Ref a) => Tree a -> Bool
isReferenceTree (Node x xs) = isRef x

pruneTree::(Ref a) => Tree a -> Tree a
pruneTree  (Node x [])
  | isHiddenTree (Node x []) = Node refEmpty []
  | otherwise = Node x []
pruneTree  (Node x subForest)
  | isHiddenTree (Node x subForest) = Node refEmpty []
  | otherwise = Node x $ map pruneTree subForest

followPath:: (Ref a) => Path -> (Tree a, Path) -> Maybe (Tree a, Path)
followPath [] _ = error "Follow path called on empty path"
followPath (x:[]) (Node y ys, p)
  | x `reference` y = Just (Node y ys, (p++[name y]))
  | otherwise = Nothing
followPath (x:xs) (Node y ys, p)
  | pathFromTopLevel (x:xs) = followPath xs (Node y ys, p ++ [name y])  
  | pathFromTopLevel (x:xs) = followPath xs (Node y ys, p ++ [name y])
  | not $ x `reference` y = Nothing
  | otherwise = oneOnly (map (followPath xs) ( zip ys (repeat (p ++ [name y]) )))

searchTree::(Ref a) => Path -> Tree a -> Maybe (Tree a, Path)
searchTree (x:xs) tree = oneOnly $ map (followPath (x:xs)) (searchPathStart x tree)

searchPathStart ::(Ref a) => Text -> Tree a -> [(Tree a, Path)]
searchPathStart x (Node y ys)
    | pathFromTopLevel [x] = [(Node y ys, [])]
--This is a depthfirst search returning all matching subtrees 
--This does not return duplicates if decendents and ancestors match
    | isRef y = []
    | x `reference` y = (Node y ys, []):(map addHeadToPath (concatMap (searchPathStart x) ys))
    | otherwise = map addHeadToPath (concatMap (searchPathStart x) ys) where
       addHeadToPath (a, b) = (a, (name y):b)


oneOnly :: [Maybe a] ->  Maybe a
oneOnly xs
    | (length $catMaybes xs) == 0 = Nothing
    | (length $catMaybes xs) == 1 = listToMaybe $catMaybes xs
    | otherwise = error $ "multiple possible trees found in OnlyOne ++ LIST:"

-- This only needs to work from the top level
-- This function should never run over hidden trees they should be pruned first
treeIntoTree :: (Ref a) => (Tree a, Path) -> Tree a -> Tree a -- It needs to be this way for currying to be the msot elegent
treeIntoTree (newSubTree, []) oldTree = error "Inserting tree with no path"
treeIntoTree (newSubTree, a:[]) (Node x xs)
  | a `reference` x && isRef x = revealHiddenTree newSubTree
  |otherwise = Node x xs
treeIntoTree (newSubTree, p:ps) (Node x xs)
    | (not (p `reference` x)) = Node x xs
    | otherwise = Node x $ map (treeIntoTree (newSubTree, ps)) xs

revealHiddenTree (Node x xs) = Node (reveal x) xs

