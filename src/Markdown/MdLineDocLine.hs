module Markdown.MdLineDocLine
  ( MdLine()
  )
where

import BasicPrelude
import           Markdown.Parser
import           Document.Types
import           Markdown.MdLine
import qualified Data.List                     as L
import           Data.List.Utils
import           ReferencedTrees.ReferencedTree
instance Eq MdLine where

  (==) a b = level a == level b

instance Ord MdLine where
  (<=) a b = level a <= level b



instance DocLine MdLine where

  toImportHeading (Heading         x y identity) = ImportedHeading x y identity
  toImportHeading (ImportedHeading x y identity) = ImportedHeading x y identity
  toImportHeading (HeadingHidden   x y identity) = ImportedHeading x y identity
  toImportHeading _ = error "toImportHeading called on wrong type"


  isLine (Line _ _ _ _) = True
  isLine _ = False

  isHeadingReference (HeadingReference _ _ _) = True
  isHeadingReference _                        = False

  getHeadingReference (HeadingReference _ _ r) = r
  getHeadingReference _ =
    error "getHeadingReference called on something other than heading reference"


  isHiddenHeading (HeadingHidden _ _ _) = True
  isHiddenHeading _                     = False

  isNotEmptyLine (Line _ "" _ _) = False
  isNotEmptyLine _               = True

  name x = Markdown.MdLine.name x

  toText = textShow

  listAppendixReferences (Line _ s appRef lineRef) = appRef
  listAppendixReferences x                         = []

  listInlineReference (Line _ s appRef lineRef) = lineRef
  listInlineReference _                         = []

  hiddenHeadingToHeading (HeadingHidden v n id) = Heading v n id
  hiddenHeadingToHeading _ =
    error "hiddenHeadingto heading called on other type of md line"

  getImport (ImportLine _ imp _) = imp
  getImport _ = error "get import called on line that is not an import"

  addId filename path (Heading v n _) =
    Heading v n (filename ++ ":" ++ (concat $ intersperse ";" path))
  addId filename path (HeadingHidden v n _) =
    HeadingHidden v n (filename ++ ":" ++ (concat $ intersperse ";" path))
  addId filename path (ImportedHeading v n _) =
    ImportedHeading v n (filename ++ ":" ++ (concat $ intersperse ";" path))
  addId filename path _ = error "Add id on non heading"

  toReference (Heading v n i) =
    Line 50 ("See [" ++ n ++ "](" ++ showIdentity i ++ ")") [] []
  toReference (HeadingHidden v n i) =
    Line 50 ("See [" ++ n ++ "](" ++ showIdentity i ++ ")") [] []
  toReference (ImportedHeading v n i) =
    Line 50 ("See [" ++ n ++ "](" ++ showIdentity i ++ ")") [] []
  toReference _ = error "To reference not called on a heading"



instance Ref MdLine where
  reference str a = Document.Types.name a == str
  hidden a = isHiddenHeading a
  isRef a = isHeadingReference a
  indent a = id a
  refEmpty = Line 50 "" [] []
  reveal (HeadingHidden v n id) = Heading v n id
  reveal x                      = x
  name x = Markdown.MdLine.name x
