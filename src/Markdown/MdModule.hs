{-# LANGUAGE AllowAmbiguousTypes #-}
module Markdown.MdModule(
   buildMdDocument,
   buildPrintableMdDocument,
   --buildHtmlMdDocument
    ) where

import Document.Modules
import System.FilePath
import Document.Types
import Markdown.MdLine
import BasicPrelude
import Data.Tree
import Data.Set(fromList, toList)
import Document.Sections
import Document.TreeSection
import Markdown.Parser
import Markdown.MdLineDocLine

mdDocumentBuilder = MarkdownDocumentBuilder

buildMdDocument::Path -> FilePath -> IO Text
buildMdDocument path filepath = buildDocument mdDocumentBuilder ["Main"] filepath 

buildPrintableMdDocument::Path -> FilePath -> IO Text
buildPrintableMdDocument path filepath = do
  print "build printabe module running"
  buildDocument mdPrintableDocumentBuilder ["Main"] filepath where
    mdPrintableDocumentBuilder = MarkdownPrintableDocumentBuilder

{--
buildHtmlMdDocument::Path->FilePath -> IO String
buildHtmlMdDocument path filepath = do
  print "build printabe module running"
  doc <- buildDocument mdHtmlDocumentBuilder ["Main"] filepath
  return doc where
    mdHtmlDocumentBuilder = MarkdownHtmlDocumentBuilder
--}
