{-# LANGUAGE OverloadedStrings #-}
module Markdown.Parser
  (
-- * Markdown lines for tree
    parserMarkdown
-- * Inline references
  , Reference(..)
  , parseReferences
  , parserAppendixReferences
  , parseNothing
  , parseAppendixReference
  , refToOriginAndPath
-- * Import 
  )
where
import qualified Text.ParserCombinators.Parsec as PC
import Text.ParserCombinators.Parsec(choice, noneOf,  Parser, ParseError, many, string, anyChar, manyTill, between,try,many1,parse,char, spaces)
import qualified Data.Text                     as T
import qualified Data.List                     as L
import           BasicPrelude
import           Data.Either
import           Document.Types
import           Markdown.MdLine

takeBetween :: Char -> Parser String
takeBetween c = do
  bef <- takeWhile' c
  ref <- takeWhile' c
  return ref where
  takeWhile' c = try (doubleParser c) <|> singleParser c   where
    doubleParser :: Char -> Parser String
    doubleParser c = do
      s   <- many (noneOf (c : []))
      end <- string $ L.take 2 $ repeat c
      return s
    singleParser c = do
      s      <- many (noneOf (c : []))
      myChar <- char c
      next   <- try (doubleParser c) <|> singleParser c
      return (s ++ (myChar : []) ++ next)

matchRef :: Parser String
matchRef = do
  a <- string "ref"
  b <- between (string "(") (string ")") $ matchingBracs
  return b

matchImage :: Parser String
matchImage = between (string "(") (string ")") $ matchingBracs

matchImageCaption :: Parser String
matchImageCaption = between (string "[") (string "]") $ matchingBracs

matchHidden :: Parser String
matchHidden = do
  a <- string "hide"
  b <- between (string "(") (string ")") $ matchingBracs
  return b

matchingBracs :: Parser String
matchingBracs = do
  a  <- many $ noneOf "()"
  b  <- many $ between (string "(") (string ")") $ matchingBracs
  b1 <- many $ noneOf "()"
  let c  = a ++ ('(' : (concat b)) ++ (')' : b1)
  let c1 = a ++ b1
  if b == [] then return $ a ++ b1 else return c

-- | This combines all the elements that parse markdown lines in a choice try combination and is applied to lines of the file before tranforming them to a tree. It then creates a parser which parsers them
parserMarkdown :: String -> Either ParseError MdLine
parserMarkdown = parse parseMarkdown " " where

  parseMarkdown :: Parser MdLine
  parseMarkdown = choice
    [ try parseImport
    , try parseHeadingReference
    , try parseHeadingHidden
    , try parseHeading
    , parseLine
    ]   where

    parseHeadingReference :: Parser MdLine
    parseHeadingReference = do
      hashes  <- fmap L.length (many1 (char '#'))
      mySpace <- spaces
      ref     <-  matchRef
      let decompRefAndPath = refToOriginAndPath ref
      let origin           = fst decompRefAndPath
      let path             = map T.pack $ snd decompRefAndPath
      let reference        = Reference origin path
      return (HeadingReference 50 (T.pack ref) reference)     where



    parseHeadingHidden :: Parser MdLine
    parseHeadingHidden = do
      hashes  <- fmap L.length (many1 (char '#'))
      mySpace <- spaces
      ref     <- fmap T.pack matchHidden
      return (HeadingHidden hashes ref "")



    parseHeading :: Parser MdLine
    parseHeading = do
      hashs <- fmap length (many1 (char '#'))
      title <- fmap (T.strip . T.pack) (many (anyChar))
      return $ Heading hashs title ""

    parseLine :: Parser MdLine
    parseLine = do
      anything <-  many (anyChar)
      let inlineReferences = fromRight (error "parseReference error")
                                       (parse parseReferences "" anything)
      let appRefs =  parserAppendixReferences anything
      return $ Line 50 (T.pack anything) appRefs inlineReferences

    parseImport :: Parser MdLine
    parseImport = do
      library  <- string "import"
      filepath <- fmap (T.strip . T.pack) (many1 (anyChar))
      return (splitImport filepath)


splitImport :: Text -> MdLine
splitImport filepath
  | L.length impAndSudo == 0 = error "No arguments"
  | L.length impAndSudo == 1 = ImportLine 50 (Import "" (impAndSudo !! 0)) ""
  | L.length impAndSudo == 2 = error
    ("only two arguments imports should be in the for a as a'")
  | otherwise = ImportLine 50 (Import sudoname before) "" where
  impAndSudo = L.map (T.unpack . T.strip) $ T.words filepath
  before     = L.unwords $ L.takeWhile (\x -> x /= "as") impAndSudo
  sudoname
    | (null $ L.dropWhile (\x -> "as" /= x) impAndSudo)
    = error $ "No sudo name" ++ show (impAndSudo)
    | otherwise
    = L.unwords $ L.tail $ L.dropWhile (\x -> x /= "as") impAndSudo

parseReferences :: Parser [Reference]
parseReferences = do
  refs <- many parseReference
  return (refs) where
  parseReference :: Parser Reference
  parseReference = do
    ref <- matchRef
    let origin = referenceToOrigin ref
    let path   = map T.pack $ snd $ refToOriginAndPath ref
    return (Reference origin path)


parserAppendixReferences :: String -> [Reference]
parserAppendixReferences l
  | isLeft parsed = error
    (show $ fromLeft (error " This right  should never run") parsed)
  | isRight parsed = fromRight (error "This left should never run") parsed where

  parsed :: Either ParseError [Reference]
  parsed = parse parseAppendixReferences "" l

parserAppendixReferences _ = []

parseAppendixReferences :: Parser [Reference]
parseAppendixReferences = do
  refs         <- many (try (parseAppendixReference))
  nothingAtend <- parseNothing
  return refs where

parseAppendixReference :: Parser Reference
parseAppendixReference = do
  beginPhrase <- manyTill anyChar (try $ string "appRef")
  b           <- between (string "(") (string ")") $ matchingBracs
  let origin = referenceToOrigin b
  let path   = map T.pack $ snd $ refToOriginAndPath b
  return (Reference origin path)

parseNothing :: Parser [Reference]
parseNothing = do
  nothing <- many (anyChar)
  return ([])


referenceToOrigin :: String -> String
referenceToOrigin s = fst (refToOriginAndPath s)

refToOriginAndPath :: String -> (String, [String])
refToOriginAndPath s
  | L.length (originAndPath s) == 0
  = error
    "Called refToPame on empty refernce Markdown.Parser: refToOriginAndPath"
  | (L.length (originAndPath s) == 1)
  = ("", decompPath $ L.head $ originAndPath s)
  | L.length (originAndPath s) == 2
  = ((L.head $ originAndPath s), decompPath $ head $ tail $ originAndPath s)
  | otherwise
  = error "2 or more : in refences from Markdown.Parser refToOriginAndPath" where

originAndPath :: String -> [String]
originAndPath s = map T.unpack $ T.splitOn (T.pack ":") $ (T.pack s)

decompPath :: String -> [String]
decompPath s = L.map (T.unpack . T.strip) $ T.splitOn (T.pack ";") $ T.pack s
