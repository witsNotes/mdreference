{-# LANGAUGE OverloadedStrings #-}
module Markdown.MdLine(
  MdLine(..), 
  showIdentity
    ) where

import Document.Types
import BasicPrelude
--import Data.List as L
import Data.List.Utils
import qualified Data.Text as T

data MdLine = 

                  Line 
                  {level ::Int
                  ,  name :: Text
                  ,  appendixReferences::[Reference]
                  ,  inlineReferences::[Reference]
                  }
                  | Heading 
                  { level ::Int
                  , name ::Text
                  , identity::Text
                  }
                  | HeadingReference 
                  { level :: Int
                    , name:: Text
                    , reference::Reference
                  }
                  | HeadingHidden
                  { level :: Int
                  , name :: Text
                  , identity :: Text
                  }
                  | ImportLine
                  { level :: Int
                   , imp :: Import
                   , name :: Text
                  }
                  | ImportedHeading
                  { level :: Int
                  , name :: Text
                  , identity:: Text
                  }

--Implemented to save time as text is more efficent 
instance Tshow MdLine where
  textShow (Line l n a i) = (n ++ "\n")
  textShow (Heading l n id) =  (T.pack (take l $ repeat ('#')) ++ " " ++ n ++ " {#"++showIdentity id++"}" ++ "\n")
  textShow (ImportLine l i m) = ( m ++ "\n")
  textShow (HeadingReference l n r) = "Heading References printed error" 
  textShow (HeadingHidden l n i) = "Hidden heading shown error"
  textShow (ImportedHeading l n i) = (T.pack (take l $ repeat ('#'))) ++ " " ++ n ++ "(import)" ++ " {#" ++ showIdentity i ++"}\n"
 
showIdentity:: Text -> Text
showIdentity s =  
  (T.replace "1" "I" . 
  T.replace "2" "II" .
  T.replace "3" "III" .
  T.replace "4" "IV" .
  T.replace "5" "V" .
  T.replace "6" "VI" .
  T.replace "7" "VII" .
  T.replace "8" "VIII" .
  T.replace "9" "IX" .
  T.replace "0" "zero" .
  T.replace "(" "leftbrac" .
  T.replace ")" "rightbrac" .
  T.replace "/" "forwardslash" .
  T.replace "-" "slash" .
  T.replace "\\" "backslash" .
  T.replace "{" "openbrace" .
  T.replace "}" "closebrace" .
  T.replace "." "dot" .
  T.replace "'" "quote" .
  T.replace ";" "semicolon" .
  T.replace ":" "fullcolon" .
  T.replace " " "space" .
  T.replace "^" "hat" .
  T.replace "$" "MATHS" ) s


showIndent:: Int -> Text
showIndent i = concat $ take i $ repeat ">"


