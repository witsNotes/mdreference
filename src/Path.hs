module Path
  (acceptedLocations,
  ) where

import System.FilePath
import System.Directory
import BasicPrelude

acceptedLocations:: IO ([FilePath])
acceptedLocations = do 
  homeDirectory <- getUserDocumentsDirectory
  return ( [homeDirectory </> "Documents" ])
