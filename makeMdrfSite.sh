#!/bin/bash
mkdir $1
cp -r ~/mdrfTemplate/* ./$1
cp ~/mdrfTemplate/.gitlab-ci.yml ./$1
cp ~/mdrfTemplate/.gitignore ./$1
cp ~/.local/bin/mdrf $1/

