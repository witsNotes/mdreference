import Test.HUnit
import Markdown.Parser
import Markdown.MdLine
import Document.Modules
import Document.Blueprints
import Document.Assembler
import Markdown.MdLineDocLine
--main :: IO ()
import Document.TreeSection
import Document.Types
import Document.Sections
import Data.Tree
import TreeTest

main = do 
  print "Tree Tests"
  runTestTT treeTests
  runTestTT tests
  runTestTT blueprintTests
  

tests = TestList [
  test1,
  buildReflessModTest,
  --buildModKitTest,
  buildRefModTest
  ]


blueprintTests = TestList [
  --buildBlueprintTest
  ]

test1 = TestCase (assertEqual "parsing plain line" (fmap Document.Types.name $ parserMarkdown "test") (Right "test"))

buildReflessModTest = TestCase(do
    doc <- (buildDocument parserMarkdown ["Main"] "./test/test1.md")::IO (String)
    file <- (readFile "./test/manualTest1.md")::IO (String)
    assertEqual "buildReferenceslessModule" doc file
    )

-- Build module with references

buildRefModTest = TestCase(do
    doc <- (buildDocument parserMarkdown ["Main"] "./test/test2.md")::IO (String)
    file <- (readFile "./test/Test2ManualRef.md")::IO (String)
    assertEqual "buildReferenceslessModule" file doc 
    )

buildModKitTest = TestCase(do
  let dir = test2MainDirection
  modKit <- (buildModuleKit dir) -- ::IO (Tree (TreeSection MdLine, [String])) 
  let mySection = (Node (Line 50 "name " [] []) [])::TreeSection MdLine
  let myModBlock = (mySection, []):: ModuleBlock (TreeSection MdLine)
  let myModKit = (Node (myModBlock, []) [])::ModuleKit (TreeSection MdLine)
  assertEqual "buildModKitTest"  myModKit modKit
    )

buildBlueprintTest = TestCase(do
  let dir = test2MainDirection
  file <- readFile "./test/test2.md"
  print $ getPotentialModuleBlueprint file dir
  blueprint <- getModuleBlueprint dir 
  main <- test2Main
  let subModDirs = [test2ReferenceDirection]
  let appendixDirections = []
  let myBlueprint = ModuleBlueprint main subModDirs appendixDirections
  assertEqual "blueprint" (myBlueprint) blueprint
    )


test2Main:: IO (TreeSection MdLine)
test2Main =  do 
  file <- readFile "./test/test2.md"
  let main = findSection file ["Main"] parserMarkdown
  return main  

test2ReferenceDirection = Direction ["Main", "Reference"] parserMarkdown "./test/test2.md" ["Reference"]

test2MainDirection = Direction [] parserMarkdown "test/test2.md" ["Main"]


