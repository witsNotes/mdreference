module Tests.ParsingTests (
) where

import Markdown.Parser
import Markdown.MdLine
import Markdown.MdLineDocLine
import Document.TreeSection
import Document.Sections
import Document.Types
--import Document.Modules
import System.IO

main::IO()
main = do
  file <- readFile "/home/jezri/Code/mdreference/src/Tests/testDoc.md"  
  parserTests
  searchSectionsTest
  --getAppandixReferencesTest
  print "Parse Markdown documents tests"
  let sections = stringToSections parserMarkdown file
  print $ sections
  print "Test searchSection"
  print $ map (searchSection ["Title 1"]) $ sections
  print "oneOnly test"
  print $ oneOnly (map (searchSection ["Title 1"] ) sections) 
  --print "oneOnly test should fail" Put in to test but couses error

  --print $ oneOnly (map (searchSection ["Title 2"] ) sections) 
 -- print "Find Section test"
  print "Get potential Sections"
  print $ getPotentialSections file ["Title 1"] parserMarkdown 
  print "Potential section"
  print $ getPotentialSection $  getPotentialSections file ["Title 1"] parserMarkdown 
  print $ ""  
  print $ "Check if there is an imports sections"
  print $ searchSections ["Imports"] sections
  print "ListMatchingImports" 
  print $ listImports sections
--
 -- print $concatMap listSectionAppendixReferences sections

parserTests:: IO()
parserTests = do 
  print "PARSER TESTS"
  print "=================================="
  print ""
  print "Parse Markdown lines test"
  print $ parserMarkdown "test" 
  print ""
  print "----------------------------------"
  print ""
  print "Parser import test"
  print $ parserMarkdown "import test1 as t1"
  print ""
  print "================================="
  print ""

searchSectionsTest::IO()
searchSectionsTest = do 
  file <- readFile "/home/jezri/Code/mdreference/src/Tests/testDoc.md"  
  let sections = stringToSections parserMarkdown file
  print "SEARCH SECTIONS"
  print "========================="
  print ""
  print $ "Check if there is an imports sections"
  print $ searchSections ["Imports"] sections
  print ""
  print "==========================="
  print ""

{--getAppandixReferencesTest::IO()
getAppandixReferencesTest = do
 let imp = Import 50 "test1" "myFileTest1" "test1"
 let string = "/home/jezri/tests"
 print "toFilePath test"
 --print $ toFilePath string imp
--}
