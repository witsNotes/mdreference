{-# LANGUAGE FlexibleInstances #-}
module TreeTest (
  treeTests
) where


import Test.HUnit
import ReferencedTrees.ReferencedTree
import Data.Tree

instance Ref String where
  reference ref main = ref == dropWhile (`elem` ['i','h', 'r']) main
  hidden s = head s == 'h' 
  isRef s = head s == 'r' 
  indent s = 'i':s
  empty = "e"
  reveal main = filter (/= 'h') main

treeTests = TestList[
 referenceTest,
 hiddenTreeTest,
 referencesTreeTest,
 pruneTreeTest,
 pruneHiddenTest,
 followPathTest,
 searchPathStartTest,
 searchPathTest0,
 searchPathTest1,
 treeIntoTreeTest
 --searchPathTest2 This one should throw an error dont know how to test for this
  ]

referenceTest = TestCase(
    assertEqual "Reference Test" (reference "a" "iia") True)

hiddenTreeTest = TestCase(
    assertEqual "Is hidden tree" (isHiddenTree (Node "ha" [Node "b" []])) True)

referencesTreeTest = TestCase(
    assertEqual "Is reference tree" (isReferenceTree (Node "ra" [Node "b" []])) True
    )

pruneHiddenTest = TestCase(
    assertEqual "pruneHiddenTree" (pruneTree (Node "ha" [Node "b" []])) (Node "e" [])
  )

pruneTreeTest = TestCase(
    assertEqual "pruneTree" (pruneTree (Node "a" [(Node "hb" []), (Node "c" [Node "hd" []])])) (Node "a" [Node "c" []])
    )

followPathTest = TestCase(
   assertEqual "follow path" (followPath ["a", "b", "c"] (Node "a" [Node "b" [Node "c" [Node "d"[]] ], Node "b" []])) $ Just (Node "c" [Node "d" []]) 
    )


searchPathStartTest = TestCase(
   assertEqual "search path start"  (searchPathStart "a" (Node "a" [Node "a" [], Node "b" []])) ([Node "a" [Node "a" [], Node "b" []], Node "a" [] ] )
    )

testTree = Node "a" [Node "a" [], Node "b" []] 

searchPathTest0 = TestCase(
   assertEqual "search path double" (searchTree ["a", "a"] testTree) $ Just (Node "a" [])
    )

searchPathTest1 = TestCase(
   assertEqual "search path from top" (searchTree ["~", "a"] testTree) $ Just (testTree)
    )

searchPathTest2 = TestCase(
   assertEqual "search path multiple" (searchTree ["a"] testTree) $ Just (testTree)
    )

treeIntoTreeTest = TestCase( 
    assertEqual "treeIntoTree" (treeIntoTree (tree2, ["a", "b"]) tree1) tree2into1
    )
main = do 
  runTestTT treeTests

tree1 = Node "a" [Node "rb" []]

tree2 = Node "b" [Node "b1" []]

tree2into1 = Node "a" [Node "b" [Node "b1" []]]
