<script> function fold(y) { var x = document.getElementById(y) ; if(x.style.display === "none"){x.style.display = "block";}else {x.style.display = "none"; } } </script><script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML' async></script><script> function fold0_0() {fold('0_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_0()"><h2>Main</h2></div>
<div class="sectionBody background-color-one" id="0_0">

main text


<script> function fold0_4_1_0() {fold('0_4_1_0')} </script><div class="import">
<div class="importHeading" onclick="fold0_4_1_0()"><h2>Reference</h2></div>
<div class="importBody background-color-two" id="0_4_1_0">

This test is in a hidden reference

<script> function fold0_3_1_4_1_0() {fold('0_3_1_4_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_3_1_4_1_0()"><h2>Hidden Reference subheading</h2></div>
<div class="sectionBody background-color-one" id="0_3_1_4_1_0">

This test is in a subheading of the hidden reference

referenced text



</div>
</div>
</div>
</div>
<script> function fold0_6_1_0() {fold('0_6_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_6_1_0()"><h2>Heading 1</h2></div>
<div class="sectionBody background-color-two" id="0_6_1_0">

This text is under heading 1



</div>
</div><script> function fold0_7_1_0() {fold('0_7_1_0')}</script><div class="section">
<div class="sectionHeading" onclick="fold0_7_1_0()"><h2>Heading 2</h2></div>
<div class="sectionBody background-color-two" id="0_7_1_0">

This text is under heading 2

<script> function fold0_3_1_7_1_0() {fold('0_3_1_7_1_0')} </script><div class="import">
<div class="importHeading" onclick="fold0_3_1_7_1_0()"><h2>Heading 1</h2></div>
<div class="importBody background-color-one" id="0_3_1_7_1_0">

This text is under heading 1



</div>
</div>





</div>
</div>
</div>
</div>

# Appendix

