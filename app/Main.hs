import           BasicPrelude
import qualified Data.Text                     as T
import qualified Data.List                     as L
import           Data.Maybe
--import           System.Environment
import           Data.Either
-- import System.Distribution
-- import System.file
-- import Text.Parsec.String (Parser)
-- import Text.Parsec.String.Combinator (many1)
-- import Text.Parsec.String.Char
-- import FunctionsAndTypesForParsing (regularParse, parseWithEof, parseWithLeftOver)
import qualified Text.ParserCombinators.Parsec as PC
import Text.ParserCombinators.Parsec(Parser, char)
import           Text.ParserCombinators.Parsec.Token
                                               as Tok
import           Text.Parsec.Language           ( haskellDef )
import           System.FilePath
import           System.Directory
import           Document.Modules
import           Markdown.Parser
import           Markdown.MdModule

data Image = Image {line :: String, path :: Maybe String}

main :: IO ()
main = do
  args <- getArgs
  let mdFileName = (args !! 0)
  let mdBaseName = (takeBaseName (T.unpack mdFileName))
  print ("Main running on: " ++ mdFileName)
  (buildMdDocument [] (T.unpack mdFileName)) >>= writeFile (mdBaseName ++ "Ref.md")
  print "Document built"
  (buildPrintableMdDocument [] (T.unpack mdFileName)) >>= writeFile (mdBaseName ++ "Print" ++ ".md")
  print "Written Printable file"
        --writeFile (mdBaseName ++ ".html") htmlText

        -- $ unpack $ myTextPics

